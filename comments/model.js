const Sequelize = require('sequelize')
const db = require('../db')
const Tickets = require('../tickets/model')
const User = require('../users/model')

const Comments = db.define(
  'comments', {
    content: Sequelize.STRING,
    author: Sequelize.STRING
  }, { timestamps: false }
)

Comments.belongsTo(Tickets)
Tickets.hasMany(Comments)

Comments.belongsTo(User)
User.hasMany(Comments)

module.exports = Comments