const Router = require('express')
const Comments = require('./model')
const auth = require('../auth/middleware')

const router = new Router()

router
  .get('/comments', (req, res, next) => {
    Comments
      .findAll()
      .then(comments => {
        res
          .status(200)
          .send(comments)
      }).catch(next)
  })

router
  .post('/comments',
    // auth, 
    (req, res, next) => {
      Comments
        .create(req.body)
        .then(newComment => {
          res
            .status(201)
            .send(newComment)
        }).catch(next)
    })

module.exports = router