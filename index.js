const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const userRouter = require('./users/router')
const eventsRouter = require('./events/router')
const ticketsRouter = require('./tickets/router')
const authRouter = require('./auth/router')
const commentsRouter = require('./comments/router')

const app = express()

const jsonParser = bodyParser.json()
app.use(cors())
app.use(jsonParser)
app.use(authRouter)
app.use(userRouter)
app.use(eventsRouter)
app.use(ticketsRouter)
app.use(commentsRouter)

const port = process.env.PORT || 5000
app.listen(port, () => console.log(`listening on port ${port}`))