const Sequelize = require('sequelize')
const db = require('../db')
const Events = require('../events/model')
const User = require('../users/model')

const Tickets = db.define(
  'tickets', {
    picture: Sequelize.STRING,
    price: Sequelize.FLOAT,
    description: Sequelize.STRING,
    author: Sequelize.STRING
  }
)

Tickets.belongsTo(Events)
Events.hasMany(Tickets)

Tickets.belongsTo(User)
User.hasMany(Tickets)

module.exports = Tickets
