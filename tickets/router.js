const Router = require('express')
const Tickets = require('./model')
const auth = require('../auth/middleware')
const Comments = require('../comments/model')

const router = new Router()

router
  .get('/tickets', (req, res, next) => {
    Tickets
      .findAll({ include: [Comments] })
      .then(tickets => {
        res
          .status(200)
          .send(tickets)
      }).catch(next)
  })

router
  .get('/tickets/:id', (req, res, next) => {
    const id = req.params.id
    Tickets
      .findByPk(id, { include: [Comments] })
      .then(ticket => {
        res
          .status(200)
          .send(ticket)
      }).catch(next)
  })

router
  .post('/tickets',
    // auth, 
    (req, res, next) => {
      Tickets
        .create(req.body)
        .then(newTicket => {
          res
            .status(201)
            .send(newTicket)
        }).catch(next)
    })

router
  .put('/tickets/:id',
    // auth, 
    (req, res, next) => {
      const id = req.params.id
      Tickets
        .findByPk(id)
        .then(ticket => {
          ticket
            .update(req.body)
            .then(updatedTicket =>
              res
                .status(202)
                .send(updatedTicket)
            ).catch(next)
        })
    })

module.exports = router