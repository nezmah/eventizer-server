const Sequelize = require('sequelize')
const db = require('../db')

const Events = db.define(
  'events', {
    name: Sequelize.STRING,
    description: Sequelize.STRING,
    picture: Sequelize.STRING,
    start_date: Sequelize.DATEONLY,
    end_date: Sequelize.DATEONLY
  }, { timestamps: false }
)

module.exports = Events