const Router = require('express')
const Events = require('./model')
const Tickets = require('../tickets/model')
const auth = require('../auth/middleware')
const Comments = require('../comments/model')

const router = new Router()

router
  .get('/events', (req, res, next) => {
    const limit = req.query.limit || 9
    const offset = req.query.offset || 0
    Events
      .count()
      .then(total => Events
        .findAll({
          limit,
          offset,
          include: [
            {
              model: Tickets,
              include: [Comments]
            }
          ]
        })
        .then(events => {
          res
            .status(200)
            .send({ events, total })
        }).catch(next))
  })

router
  .get('/events/:id', (req, res, next) => {
    const id = req.params.id
    Events
      .findByPk(id, {
        include: [
          {
            model: Tickets,
            include: [Comments]
          }
        ]
      })
      .then(event => {
        res
          .status(200)
          .send(event)
      }).catch(next)
  })

router
  .post('/events',
    // auth, 
    (req, res, next) => {
      Events
        .create(req.body)
        .then(newEvent => {
          res
            .status(201)
            .send(newEvent)
        }).catch(next)
    })

router
  .put('/events/:id', (req, res, next) => {
    const id = req.params.id

    Events
      .findByPk(id)
      .then(event => {
        event
          .update(req.body)
          .then(updatedEvent =>
            res
              .status(202)
              .send(updatedEvent)
          ).catch(next)
      })
  })

module.exports = router