const { Router } = require('express');
const { toJWT } = require('./jwt');
const bcrypt = require('bcrypt');
const User = require('../users/model');

const router = new Router();

router.post('/login', (req, res, next) => {
  const username = req.body.username
  const password = req.body.password

  if (!username || !password) {
    res.status(400).send({
      message: 'Please supply a valid email and password'
    })
  } else {
    User
      .findOne({
        where: {
          username: username
        }
      })
      .then(entity => {
        if (!entity) {
          res
            .status(400)
            .send({ message: 'Userneme or password incorrect' })
          return
        }
        if (bcrypt.compareSync(req.body.password, entity.password)) {
          res.send({ jwt: toJWT({ userId: entity.id }), name: req.body.username, id: entity.id })
        }
        else {
          res
            .status(400)
            .send({ message: 'Incorrect Password ' })
        }
      })
      .catch(next)
  }
})

module.exports = router;
