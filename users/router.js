const Router = require('express')
const User = require('./model')
const bcrypt = require('bcrypt')
const auth = require('../auth/middleware');
const Tickets = require('../tickets/model')

const router = new Router()

router.post(
  '/register',
  (req, res, next) => {
    const newUser = {
      username: req.body.username,
      password: req.body.password,
      password_confirmation: req.body.password_confirmation
    }
    if (newUser.username && newUser.password && newUser.password_confirmation) {
      newUser.password = bcrypt.hashSync(req.body.password, 10)
      if (bcrypt.compareSync(newUser.password_confirmation, newUser.password)) {
        User
          .findOne({
            where: {
              username: newUser.username
            }
          })
          .then(user => {
            if (!user) {
              User
                .create(newUser)
                .then(user => {
                  res
                    .status(201)
                    .send({
                      message: 'User created successfully',
                      username: user.username,
                      user_id: user.id
                    })
                })
            } else if (user.username === newUser.username) {
              res
                .status(406)
                .send({
                  message: `Username ${newUser.username} already taken`
                })
            }
          }).catch(next)
      } else {
        res
          .status(422)
          .send({
            message: "Password doesn't match"
          })
      }
    } else {
      res
        .status(400)
        .send({
          message: 'Please fill in all fields'
        })
    }
  })

router
  .get(
    '/users',
    (req, res, next) => {
      User
        .findAll({ include: [Tickets] })
        .then(users => {
          res
            .status(200)
            .send(users)
        }).catch(next)
    }
  )

router
  .get(
    '/users/:id',
    (req, res, next) => {
      const id = req.params.id
      User
        .findByPk(id, { include: [Tickets] })
        .then(user => {
          res
            .status(200)
            .send(user)
        }).catch(next)
    }
  )

module.exports = router
