# Eventizer-server

You can try deployed API on [Heroku](https://eventizer-server.herokuapp.com)

## What this project is about

This is a server side of a 'Ticket selling' app written in JavaScript using Node, Express, Postgres and Sequelize. It was a final assignment for graduating [the Codaisseuer Academy](https://codaisseur.com/become-a-developer). Check out the client-side of this project [here](https://bitbucket.org/nezmah/eventizer-client/src/master/)

Visitor is able to see all ongoing events and tickets for those events.
Visitor needs to register in order to do more than just watching the content of the site. Only registered user can be logged in. Logged user can
- create a new event - create a ticket for an event - comment on tickets - edit name, price and description of ticket that s/he created

## Table of contents:
- **[Technologies used](#technologies-used)**
- **[Goals for this project](#goals-for-this-project)**
- **[How to install](#how-to-install)**
- **[Features built so far](#features-built-so-far)**
- **[Endpoints used in API](#endpoints-in-this-API)**

## Technologies used

#### 👇 Click links to view some samples in this project

- **[PostgresQL](./auth/router.js)**
- **[JavaScript](./index.js)**
- **[Express](./db.js)**
- **[Token Authentication](./auth/jwt.js)**
- **[BcryptJS](./users/router.js)**
- **[Sequelize](./events/model.js)**

## Goals for this project:
- To prove that I am ready for work on real world project
- To combine front-end and backend apps in one working project
- To practice disciplined git usage
- To give best of myself in short time and create fully working app rich in features

## How to install
- clone this repo
- run the postgres db on port 1234 with username 'postres' and password 'secret'
- `npm install`
- `npm start`

#### If you want to se try full working app
- clone the client repo
- open new terminal
- `npm install`
- `npm start`
- it should open your browser on port localhost:3000 with the working app :)

## Features built so far:

- **[Create DB for Users](./users)**
- **[Create DB for Events](./events)**
- **[Create DB for Tickets](./tickets)**
- **[Create DB for Comkments](./comments)**
- **[Create server](./index.js)**
- **[Build Authentication](./auth)**

## Endpoints in this API
These are the available endpoints of the API `@root : http://mywebsite.com`.  

* **POST @root/register**:  
    Registers a new User.
    
* **GET @root/users**:  
   Returns a list of all registered Users.
    
* **GET @root/users/:id**:  
    Returns a single registered User.
    
* **POST @root/users/:id**:  
    Loggs in a specific registered User.

* **GET @root/events**:  
    Returns a lit of all Events.

* **GET @root/events/:id**:  
    Returns a single Event.

* **POST @root/events**:  
    Creates a new Event.

* **PUT @root/events/:id**:  
    Edits an single Event (name, description, date).

* **GET @root/tickets**:  
    Returns a list of all Tickets for specific Event including belonging Comments.

* **GET @root/tickets/:id**:  
    Returns a single Tickets including belonging Comments.

* **POST @root/tickets/:id**:  
    Creates a new Ticket.

* **PUT @root/tickets/:id**:  
    Edits a specific Ticket (name, description, price).

* **GET @root/comments**:  
    Returns a list of all Comments for specific Ticket.

* **POST @root/comments**:  
    Creates a new Comment for Specific Ticket.